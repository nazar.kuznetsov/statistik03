import React from 'react';
import Select from 'react-select';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import NoSsr from '@material-ui/core/NoSsr';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import MenuItem from '@material-ui/core/MenuItem';
import { emphasize } from '@material-ui/core/styles/colorManipulator';

const styles = theme => ({
  root: {
    flexGrow: 1,
    height: 250,
    margin: '10px 0'
  },
  input: {
    display: 'flex',
    padding: 0
  },
  valueContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    flex: 1,
    alignItems: 'center',
    overflow: 'hidden'
  },
  chip: {
    margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit / 4}px`,
  },
  chipFocused: {
    backgroundColor: emphasize(
      theme.palette.type === 'light' ? theme.palette.grey[300] : theme.palette.grey[700],
      0.08,
    )
  },
  noOptionsMessage: {
    padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px`,
  },
  singleValue: {
    fontSize: 16
  },
  placeholder: {
    position: 'absolute',
    left: 2,
    fontSize: 16
  },
  paper: {
    position: 'absolute',
    zIndex: 1,
    marginTop: theme.spacing.unit,
    left: 0,
    right: 0
  },
  divider: {
    height: theme.spacing.unit * 2,
  }
});

interface INoOptionsMessage {
  children: object;
  innerProps: object;
  selectProps: {
    textFieldProps?: string;
    classes: {
      noOptionsMessage: string;
      input?: string;
    }
  };
  innerRef?: string;
}

function NoOptionsMessage(props: INoOptionsMessage) {
  return (
    <Typography
      color="textSecondary"
      className={props.selectProps.classes.noOptionsMessage}
      {...props.innerProps}
    >
      {props.children}
    </Typography>
  );
}

function inputComponent({ inputRef, ...props }) {
  return <div ref={inputRef} {...props} />;
}

function Control(props: INoOptionsMessage) {
  return (
    <TextField
      fullWidth={true}
      InputProps={{
        inputComponent,
        inputProps: {
          className: props.selectProps.classes.input,
          inputRef: props.innerRef,
          children: props.children,
          ...props.innerProps
        },
      }}
      {...props.selectProps.textFieldProps}
    />
  );
}

interface IOption {
  innerRef: any;
  isFocused: boolean;
  isSelected: number;
  innerProps: any;
  children: any;
}

function Option(props: IOption) {
  return (
    <MenuItem
      buttonRef={props.innerRef}
      selected={props.isFocused}
      component="div"
      style={{
        fontWeight: props.isSelected ? 500 : 400,
      }}
      {...props.innerProps}
    >
      {props.children}
    </MenuItem>
  );
}

interface IPlaceholder {
  selectProps: {
    classes: {
      placeholder: string;
      singleValue?: string;
      valueContainer?: string;
    }
  };
  innerProps?: any;
  children?: any;
}

function Placeholder(props: IPlaceholder) {
  return (
    <Typography
      color="textSecondary"
      className={props.selectProps.classes.placeholder}
      {...props.innerProps}
    >
      {props.children}
    </Typography>
  );
}

const SingleValue = (props: IPlaceholder) => {
  return (
    <Typography className={props.selectProps.classes.singleValue} {...props.innerProps}>
      {props.children}
    </Typography>
  );
};

const ValueContainer = (props: IPlaceholder) => {
  return <div className={props.selectProps.classes.valueContainer}>{props.children}</div>;
};

interface IMenu {
  children: any;
  innerProps: any;
  selectProps: {
    classes: {
      paper: string;
    }
  };
}

const Menu = (props: IMenu) => {
  return (
    <Paper square={true} className={props.selectProps.classes.paper} {...props.innerProps}>
      {props.children}
    </Paper>
  );
};

const components = {
  Control,
  Menu,
  NoOptionsMessage,
  Option,
  Placeholder,
  SingleValue,
  ValueContainer
};

interface IData {
  label: string;
}

interface IProps {
  label: string;
  placeholder: string;
  showing: IData[];
}

interface IInput {
  margin: number;
  paddingBottom: number;
  paddingTop: number;
  visibility: string;
  color: string;
  boxSizing: string;
}

class IntegrationReactSelect extends React.Component<IProps> {
  state = {
    single: null,
    multi: null
  };

  handleChange = (name: string) => (value: string) => {
    this.setState({ [name]: value });
  }

  render() {
    const { classes, theme } = this.props;

    const selectStyles = {
      input: (base: IInput) => ({
        ...base,
        color: theme.palette.text.primary,
        '& input': {
          font: 'inherit'
        }
      })
    };

    return (
      <div className={classes.root}>
        <NoSsr>
          <Select
            classes={classes}
            styles={selectStyles}
            options={this.props.showing}
            components={components}
            value={this.state.single}
            textFieldProps={{
              label: this.props.label,
              InputLabelProps: {
                shrink: true
              }
            }}
            onChange={this.handleChange('single')}
            placeholder={this.props.placeholder}
            isClearable={true}
          />
          <div className={classes.divider} />
        </NoSsr>
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(IntegrationReactSelect);
