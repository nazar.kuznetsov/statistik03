import {combineReducers} from 'redux';
import {mockReducer} from '../modules/albums/controllers/services/reducers';
import reducer from '../modules/feedback-consolidation/service/reducer';

const rootReducer = combineReducers({
    mockReducer,
    reducer
});

export default rootReducer;
