import {validateAction} from './utils/validate-action';
import {CALL_API} from './constants';

export default (
    error: { httpStatus: string, message: string },
    callApi: any
) => (store: any) => (next: any) => (action: any) => {
    let callFunc: any;
    let callName: any;

    if (action[CALL_API]) {
        callName = CALL_API;
        callFunc = callApi;
    } else {
        return next(action);
    }

    const callInfo = action[callName];

    function actionWith(data: {}) {
        const finalAction = {...action, ...data};
        // @ts-ignore
        delete finalAction[callName];
        return finalAction;
    }

    validateAction(callInfo);

    const [requestType, successType, failureType] = callInfo.types;
    next(actionWith({type: requestType}));

    return callFunc(callInfo, store).then(
        (response: any) => next(actionWith({
            type: successType,
            ...response ? {response} : {}
        })),
        (e: any) => {
            const errorMsg = Array.isArray(e) ? e.join(' ') : e || error;
            return next(actionWith({
                type: failureType,
                error: errorMsg
            }));
        }
    );
};
