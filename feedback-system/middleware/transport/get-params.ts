interface Input {
    url: string;
    params: {
        key?: string
    };
}

interface IOutput {
    url: string;
}

export default ({url, params}: Input): IOutput => {
    if (Object.keys(params).length) {
        const entries = ((Object.entries(params)))
            .filter(([key, value]) => value !== undefined && value !== null)
            .map(([key, value]) => `${key}=${prepareValues(value)}`)
            .join('&');
        if (entries) {
            url += '?' + entries;
        }
    }
    return {url};
};

const prepareValues = value => (Array.isArray(value) ? value : [value]).map(encodeURIComponent).join();
