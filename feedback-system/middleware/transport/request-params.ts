import {CONTENT_TYPE_FORM_DATA} from '../constants';

const prepareRequestQuery = (data: object) => {
    return ((Object.entries(data)))
        .filter(([key, value]) => value !== undefined && value !== null)
        .map(([key, value]) => encodeURIComponent(key) + '=' + encodeURIComponent(value))
        .join('&');
};

interface Input {
    headers: { [key: string]: string };
    params: object;
}

interface IOutput {
    headers: { [key: string]: string };
    body: string;
}

export default ({headers, params}: Input): IOutput => ({
    headers: {
        ...headers,
        'Content-Type': CONTENT_TYPE_FORM_DATA
    },
    body: prepareRequestQuery(params)
});
