import { put, takeEvery } from 'redux-saga/effects';
import { MOCKED_ACTION_SUCCESS, MOCKED_ACTION_REQUEST } from './constants';

function* mockedSetData() {
    yield put({ type: MOCKED_ACTION_SUCCESS });
}

function* mockedSaga() {
    yield takeEvery(MOCKED_ACTION_REQUEST, mockedSetData);
}

function* fetchUser(action) {
    try {
        const user = yield call(fetch, 'user');
        yield put({ type: "USER_FETCH_SUCCEEDED", user: user });
    } catch (e) {
        yield put({ type: "USER_FETCH_FAILED", message: e.message });
    }
}

function* mySaga() {
    yield takeEvery("USER_FETCH_REQUESTED", fetchUser);
}


export default mockedSaga;
