import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Layout from './layout';
import FeedbackConsolidation from '../feedback-consolidation';
import QualityFeedback from '../quality-feedback';
import Statistic from '../statistic';
import NotFound from '../not-found';

const MainContainer = () => {
    return (
        <Layout>
            <Switch>
                <Route path={'/'} component={QualityFeedback} exact={true}/>
                <Route path={'/feedback-consolidation'} component={FeedbackConsolidation}/>
                <Route path={'/statistic'} component={Statistic}/>
                <Route component={NotFound}/>
            </Switch>
        </Layout>
    );
};

export default MainContainer;
