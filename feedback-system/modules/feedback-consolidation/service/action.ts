import { takeEvery } from 'redux-saga/effects';

function* fetchUser(action) {
    console.log(action);
    // fetch('https://images.dog.ceo/breeds/weimaraner/n02092339_6212.jpg');
}

// export function* watchFetchUser() {
//     { // добавилось (
//         type: 'MY_TEST_ACTION',
//       }
//     yield takeEvery('FEEDBACK_ADD', fetchUser);
// }


function* watchFetchUser() {
    yield takeEvery('MOCKED_ACTION_REQUEST', fetchUser);
  }