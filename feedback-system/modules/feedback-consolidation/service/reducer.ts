import { api } from './api';
import {
    FEEDBACK_DELETE,
    FEEDBACK_ADD,
    FEEDBACK_UPDATE
} from './constans';

const initialState = {
    feedback: api
};

export default (state = initialState, action) => {
    switch (action.type) {
      case FEEDBACK_DELETE:
        return {
          ...state, feedback: state.feedback.filter(element => element.id !== action.payload.id)
        };
        case FEEDBACK_ADD:
        return {
          ...state, feedback: [action.payload, ...state.feedback]
        };
      default: return state;
    }
  };
