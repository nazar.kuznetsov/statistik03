import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import {watchFetchUser} from './service/action';

class FeedbackConsolidation extends Component {
  state = {
    value: ''
  };

  handleChange = ({target}) => {
   this.setState({
     value: target.value
   });
  }

  handleClick = () => {
    this.props.watchFetchUser();
  }

  render() {
    const { list } = this.props;
    return (
      <>
      <ul>
        {
          list.map(element => {
            return (
              <li key={element.id}>{element.nameTo}</li>
            );
          })
        }
      </ul>
      <input value={this.state.value} onChange={this.handleChange} type="text"/>
      <button onClick={this.handleClick}>Добавить</button>
      </>
    )
  }
}

const mapStateToProps = ({ reducer }) => {

  return {
    list: reducer.feedback
  };
};

const mapDispatchToProps = dispatch => {
  return {
    watchFetchUser: () => dispatch(watchFetchUser())
  };
};

export default compose(
  connect(mapStateToProps, mapDispatchToProps)
)(FeedbackConsolidation);
