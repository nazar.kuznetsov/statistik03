import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Feedback from './feedback';
import Title from './title';
import { TableCell, TextField } from '@material-ui/core';

interface IFeedback {
  nameTo: string;
  nameFrom: string;
  rating: number;
  comments: string;
  id: string;
}

interface ITheme {
  root: {
    width: string,
    marginTop: number,
    overflowX: string
  };
  table: {
    minWidth: number
  };
  spacing: {
    unit: number;
  };
}

interface IProps {
  classes: {
    root: string;
    table: string;
  };
}

interface IState {
  orderBy: string;
  data: IFeedback[];
  direction: 'asc' | 'desc';
}

/* Table title
================================================================================= */
const title = [
  { id: 'nameTo', label: 'От кого', numeric: false },
  { id: 'nameFrom', label: 'Кому', numeric: false },
  { id: 'rating', label: 'Оценка', numeric: false },
  { id: 'comments', label: 'Коментарий', numeric: false }
];

/* Data
================================================================================= */
const data: IFeedback[] = [
  {
    nameTo: 'Андрей  Кличко',
    id: '001',
    nameFrom: 'Тлекс Дрон',
    rating: 3,
    comments: 'Ок'
  },
  {
    nameTo: 'Петро  Порошенко',
    id: '002',
    rating: 2,
    nameFrom: 'Макс Опг',
    comments: 'Молодец'
  },
  {
    nameTo: 'Виктор  Янукович',
    id: '003',
    nameFrom: 'Алкоголик Черный',
    rating: 1,
    comments: 'Плохо'
  },
  {
    nameTo: 'Аладимир Путин',
    id: '004',
    nameFrom: 'Янукович Виктор',
    rating: 4,
    comments: 'Молодец'
  },
  {
    nameTo: 'Барак Обама',
    id: '005',
    rating: 8,
    nameFrom: 'Космос Олеп',
    comments: 'Класс'
  },
  {
    nameTo: 'Ким Чен Ир',
    id: '006',
    rating: 7,
    nameFrom: 'Владимир Кличко',
    comments: 'Круто'
  }
];

const styles = (theme: ITheme) => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto'
  },
  table: {
    minWidth: 700
  }
});

class SimpleTable extends Component<IProps, IState> {
  state = {
    direction: 'desc'
  };

  sorting = (type: string) => {
    // const clone = [...this.state.data];
    // const types = this.state.orderBy;
    // let sort;
    // if (this.state.direction === 'desc') {
    //   sort = type === 'string'
    //     ? (a: IFeedback, b: IFeedback) => a[types].localeCompare(b[types])
    //     : (a: IFeedback, b: IFeedback) => a[types] - b[types];

    // } else {
    //   sort = type === 'string'
    //     ? (a: IFeedback, b: IFeedback) => b[types].localeCompare(a[types])
    //     : (a: IFeedback, b: IFeedback) => b[types] - a[types];
    // }

    // const feedback = clone.sort(sort);
    // this.setState({ data: feedback });

  }

  derection = (name: string, type: 'string' | 'number') => {
    this.setState({
      orderBy: name
    },
      () => this.sorting(type)
    );
  }

  /* Sorting On Click Title
  ================================================================================= */
  handleClick = (id: string) => {
    const type = id === 'rating' ? 'number' : 'string';
    this.setState((state) => {
      let result = '';
      if (state.orderBy === id) {
        result = state.direction === 'desc' ? 'asc' : 'desc';
      } else {
        result = 'desc';
      }
      return {
        direction: result
      };
    }, () => this.derection(id, type));
  }

  /* Delete feedback
  ================================================================================= */
  delete = (id: string) => {
    const feedback = [...this.state.data].filter(element => element.id !== id);
    this.setState({ data: feedback });
  }

  componentDidMount() {
    this.sorting('string');
  }

  handleChange = ({target}) => {
    console.log(target.value)
  }

  sortingRating = () => {
    this.setState(({direction}) => {
      return {direction: direction === 'desc' ? 'asc' : 'desc'};
    }, () => this.props.sortingRating(this.state.direction));

  }

  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
             <Table className={classes.table}>
              <TableHead>
                <TableRow>
                  <TableCell>
                    <TextField
                      id="standard-dense"
                      label="Автор"
                      onChange={this.props.filterAuthor}
                    />
                  </TableCell>
                  <TableCell>
                    <TextField
                      id="standard-dense2"
                      label="Пользователь"
                      onChange={this.props.filterUser}
                    />
                  </TableCell>
                  <Title
                    handleClick={this.sortingRating}/>
                  <TableCell>Комментарий</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {this.props.data.map(row => (
                  <TableRow key={row.id}>
                    <Feedback
                      author={row.author}
                      name={row.name}
                      rating={row.rating}
                      id={row.id}
                      comments={row.comments}
                      onDelete={this.delete}
                    />
                  </TableRow>
                ))
                }
              </TableBody>
            </Table>
      </Paper>
    );
  }
}

export default withStyles(styles)(SimpleTable);
