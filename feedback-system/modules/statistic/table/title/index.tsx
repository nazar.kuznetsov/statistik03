import React from 'react';
import { TableCell, TableSortLabel } from '@material-ui/core';

interface ITitle {
    label: string;
    orderBy: string;
    direction: 'asc' | 'desc';
    handleClick: (id: string) => void;
    id: string;
}

const Title = ({ label, handleClick, id, orderBy, direction }: ITitle) => {
    const click = () => {
        handleClick(id);
    };

    return (
        <TableCell
            onClick={click}>{label}
            <TableSortLabel
                active={orderBy === id}
                direction={direction}
            />
        </TableCell>
    );
};

export default Title;
