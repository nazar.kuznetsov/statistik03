import React, { Component } from 'react';
import Table from './table';
import { Autocomplete } from '../../components';

const suggestions = [
  { label: 'Afghanistan1' },
  { label: 'Aland Islands' },
  { label: 'Albania 2' },
  { label: 'Algeria' },
  { label: 'American Samoa' },
  { label: 'Andorra' },
  { label: 'Angola' },
  { label: 'Anguilla' },
  { label: 'Antarctica' },
  { label: 'Antigua and Barbuda' },
  { label: 'Argentina' },
  { label: 'Armenia' },
  { label: 'Aruba' },
  { label: 'Australia' },
  { label: 'Austria' },
  { label: 'Azerbaijan' },
  { label: 'Bahamas' },
  { label: 'Bahrain' },
  { label: 'Bangladesh' },
  { label: 'Barbados' },
  { label: 'Belarus' },
  { label: 'Belgium' },
  { label: 'Belize' },
  { label: 'Benin' },
  { label: 'Bermuda' },
  { label: 'Bhutan' },
  { label: 'Bolivia, Plurinational State of' },
  { label: 'Bonaire, Sint Eustatius and Saba' },
  { label: 'Bosnia and Herzegovina' },
  { label: 'Botswana' },
  { label: 'Bouvet Island' },
  { label: 'Brazil' },
  { label: 'British Indian Ocean Territory' },
  { label: 'Brunei Darussalam' }
];

interface IFeedback {
  author: string;
  name: string;
  rating: number;
  comments: string;
  id: string;
}

const api: IFeedback[] = [
  {
    author: 'Андрей Кличко',
    id: '001',
    name: 'Тлекс Дрон',
    rating: 3,
    comments: 'Ок'
  },
  {
    author: 'Петро Порошенко',
    id: '002',
    rating: 2,
    name: 'Макс Опг',
    comments: 'Молодец'
  },
  {
    author: 'Виктор Янукович',
    id: '003',
    name: 'Алкоголик Черный',
    rating: 1,
    comments: 'Плохо'
  },
  {
    author: 'Аладимир Путин',
    id: '004',
    name: 'Янукович Виктор',
    rating: 4,
    comments: 'Молодец'
  },
  {
    author: 'Барак Обама',
    id: '005',
    rating: 8,
    name: 'Космос Олеп',
    comments: 'Класс'
  },
  {
    author: 'Ким Чен Ир',
    id: '006',
    rating: 7,
    name: 'Владимир Кличко',
    comments: 'Круто'
  }
];

export class Statistic extends Component {
  state = {
    user: '',
    author: '',
    derection: ''
  };

  /* Значение для поиска user
  ====================================================================== */
  setFilterUser = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ user: e.target.value.toLowerCase()});
  }

  /* Значение для поиска author
  ====================================================================== */
  setFilterAuthor = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ author: e.target.value.toLowerCase()});
  }

  /* Удалить элемент из таблицы
  ====================================================================== */
  delete = () => {
    // удалить
  }

  /* Вернуть обратно от куда пришел
  ====================================================================== */
  restore = () => {
    // вернуть
  }

  sortingRating = (type: string) => {
    this.setState({ derection: type });
  }

  /* Сортировать оценки
  ====================================================================== */
  derection = (data: IFeedback[]) => {
    switch (this.state.derection) {
      case 'desc':
        return [...data].sort((a, b) => a.rating - b.rating);
      case 'asc':
        return [...data].sort((a, b) => b.rating - a.rating);
      default: return data;
    }
  }

  render() {
    let data = api
    .filter(element => element.name.toLowerCase().includes(this.state.user))
    .filter(element => element.author.toLowerCase().includes(this.state.author));

    data = this.derection(data);

    return (
      <>
        <Table
          data={data}
          sortingRating={this.sortingRating}
          filterUser={this.setFilterUser}
          filterAuthor={this.setFilterAuthor}
        />

        <Autocomplete
          label={'Название'}
          showing={suggestions}
          placeholder={'Placeholder'}
        />
      </>
    );
  }
}

export default Statistic;
