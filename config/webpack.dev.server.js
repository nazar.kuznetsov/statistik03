const path = require('path');
const webpackMerge = require('webpack-merge');
const commonConfig = require('./webpack.dev.config');

module.exports = webpackMerge(commonConfig, {
    devServer: {
        disableHostCheck: true,
        port: process.env.PORT || 3000,
        hot: false,
        historyApiFallback: true,
        contentBase: path.resolve(),
        watchOptions: {
            ignored: /node_modules/,
            aggregateTimeout: 300,
            poll: 1000
        }
    }
});
